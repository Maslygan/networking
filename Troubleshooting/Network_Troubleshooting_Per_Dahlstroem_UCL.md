---
title: 'Network troubleshooting tools'
subtitle: 'Linux and Windows'
authors: ['Per Dahlstrøm \<pda@ucl.dk>']
main_author: 'Per Dahlstrøm'
date: \today
email: 'pda@ucl.dk'
left-header: \today
right-header: 'Network troubleshooting tools'
---

# Audience

Students at the two year IT Technology education at UCL in Odense Denmark.

---

# Purpose

The purpose of this document is to provide students with a compilation of the most often used network tools for troubleshooting. Most tools are for Linux but in some instances also for Windows. This document  lists the tools in the form of concrete usage examples and in some cases also with a short explanation and some background knowledge.

---

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

# Example network

This generic network diagram serves as the foundation for describing network communication problems and their possible cure and also to demonstrate some networking tools.

The network consists of three routes R1, R2 and R3. R1 and R2 are interconnected through network 10.10.10.0/24. R2 and R3 are interconnected through network 10.56.16.0/22. This latter network is the UCL school network.


![Semantic description of image](Genuine_Network_PDA_V02.JPG "Generic network diagram")  
* Figure 1 Generic network diagram*  

---

# Trouble shooting tools and example use on Linux

# man

* `man ping`

Display the manual for any program by the man command. E.g. here the manual for the ping program.

---

# ping  

ping comes included in all Linux distribution. (I believe?)

Check for connectivity at layer 3 between two devices. Ping has a number of options that can help pinpoint a networking problem.
As an example, ping from PC1 to PC2. Note that PC1 and PC2 are on two different networks. The R1 router must thus hold a route to the 192.168.11.0 network for the ping to succeede.

* `ping -n 192.168.11.3`  
   Ping 192.168.11.3 without name resolution in the displayed output.
* `ping -I eth1 192.168.11.3`  
      Force ping out on interface eth1
* `ping -D -O -q -r -I eth0 192.168.11.3`  
      -I: Force ping out on interface eth0  
      -D: Print timestamp before each line.  
      -O: Report  outstanding  ICMP  ECHO reply before sending next packet.  
      -q: Quiet output.  Nothing is displayed except the summary lines at startup time and when finished.  
      -r:  Bypass the normal routing tables and send directly to a host on an attached interface.  

The ping program sends ping requests to a device at the specified IP address and awaits ping replies. Ping does this by sending Internet Control Message Protocol (ICMP) echo request packets to the destination host and waits for an ICMP reply. Ping also measures the time it takes for the packets to return.
The ping program gives different ICMP messages depending on if ping requests could be send or not and wether ping replies were received or not.

Here are some of the ping outputs and a brief explanation of possible causes:

* ICMP message: `Destination Host Unreachable`
    * Examples of possible causes:  
            * Destination Host is turned off  
            * No connection to Destination host. E.g. no cable to Destination host.  
            * Destination Host has a misconfigured default gateway IP address.  
            * Destination Host has a misconfigured IP address.   
            * Destination Host has a misconfigured subnet mask.  
            * Gate way router does not have a route to Destination host.??  
<br>

* ICMP message: `No route to host`
    * Examples of possible causes:  
            * Ping could not be send as there is no default gateway configured.  
            * Ping could not be send as there is no IP address configured.  
            * Ping could not be send as there is no route in the routing table that matches the destination network. Check the routes in the routing table.  
<br>

* ICMP message: `Request Timed Out`
    * Examples of possible causes:  
            * Heavy traffic on involved networks.  
            * Also the same causes as Destination Host Unreachable.  
<br>

* ICMP message: No ICMP output message from ping at all.  
 
    * Examples of possible causes:  
            * This can be caused by a cable not being plugged into that interface, i.e. the interface is down. Monitoring with `tcpdum` also shows no traffic at all comming out on the relevant interface.  

* `ping 255.255.255.255 or e.g. ping 192.168.10.255`  
      If this is run on PC1 it will ping all devices in the same broadcast domain as PC1. All devices will reply, but only the one replying the fastest will be shown in the output. In the actual case only R1 will reply. Replies from all devices can be monitored with e.g. tcpdump.  



---

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

# arping

`sudo apt install arping -y`

* `arping 192.168.12.1`  
Force arp requests to 192.168.12.1. This can be used as a "ping" to Junos devices which do not forward ICMP/ping to the controle plane.  
Use `monitor traffic detail layer2-headers` on Junos device or just `monitor traffic`  

* `arping -c 3 192.168.12.1`  
Only send 3 arps.  

Sources:

* https://www.poftut.com/arping-command-tutorial-examples-linux/


---

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

# tcpdump

`sudo apt-get install tcpdump`

Also see Wireshark.

Spy on all traffic going out of and into an interface. Tcpdump is an indispensable tool in trouble shooting. It gives eyes to see what is going on on a network.

* `tcpdump -i ens33 -en icmp`  
    - i: Specify the Interface to listen on or monitor.  
    - ens33: Is here the example interface to be monitored.  
    - en: Show both mac and IP addresses.  
    - icmp: show only packets based on the icmp protocol.
* `tcpdump -i ens33 -en icmp or arp`  
Display icmp and arp traffic with IP and mac addresses.
* `tcpdump port 80 -w myCaptureFile.pcap`  
Capture traffic to port 80 and save in myCaptureFile.pcap.
Use e.g. Wireshark to inspect the captured packets graphically.  

Sources:  

* http://edoceo.com/cli/tcpdump
* https://www.thegeekstuff.com/2010/08/tcpdump-command-examples
* https://danielmiessler.com/study/tcpdump

---

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

# route

route comes included in all Linux distribution.

Also see `ìp`

The route command lists what routes a device knows of. Every ethernet connected device has a  routing table. Also LapTops.  
A common error is to have multiple default gateways DGW. This will be revealed when listing the routing table. 

* `route -n`  
  List routing table without name resolution.  

Routing table example:

|Destination|Gateway|Genmask|Flags|Metric|Ref|Use|Iface|
|-----------|-------|-------|-----|------|---|---|-----|  
|0.0.0.0|192.168.2.10|0.0.0.0|UG|203|0|0|eth1|
|169.254.0.0|0.0.0.0|255.255.0.0|U|1000|0|0|eth1|
|192.168.2.0|0.0.0.0|255.255.255.0|U|203|0|0|eth1|  

or

* `route`  
  List routing table with name resolution. 

Routing table example:

|Destination|Gateway|Genmask|Flags|Metric|Ref|Use|Iface|
|-----------|-------|-------|-----|------|---|---|-----|  
|default|gateway|0.0.0.0|UG|203|0|0|eth1|
|link-local|0.0.0.0|255.255.0.0|U|1000|0|0|eth1|
|192.168.2.0|0.0.0.0|255.255.255.0|U|203|0|0|eth1| 

The two tables are identical.

If `route` is used without the `-n` option, Linux will try to resolve IP adresses to domain names and this can take long time and thus a long delay for the table to be displayed.

* Table column explanations. The two tables are identical.

    * Destination:  
The IP ID of a destination network or the IP of a destination host. In the above example 0.0.0.0 means that any destination IP address matches this route. Or in other words; send IP packets which do not match any other destination entry to 192.168.2.10, which thus is the default gate way here.
    * Gateway:  
In the two tables above is shown that the default route is to gateway at IP address 192.168.2.10.

    * Genmask: 
Shown the network mask for the network. 0.0.0.0 means match any network.

    * Flags: 
        * U means the route is up.  
        * G means that specified gateway should be used for this route.  
        * D dynamically installed.  
        * M modified.  
        * R reinstated.  
        * ! rejecting route.  

    * Metric:
    The distance to the target, usually counted in hops.

    * Ref:
    Number of references to this route but not used in the Linux kernel.  

    * Use:  
    Count of lookups for the route.

    * Iface:  
    Interface to which packets for this route will be sent.

* `route add default gw 192.168.1.254 eth0`  
Route all traffic via 192.168.1.254 gateway connected via eth0.

* `route del -net 0.0.0.0 gw 192.168.2.10 netmask 0.0.0.0 dev eth1`  
Delete a route from the routing table.


The link-local or the 169.254.0.0/16 network is used for Automatic Private IP Addressing, or APIPA. If a DHCP client attempts to get an address, but fails to find a DHCP server after the timeout and retries period it will randomly assume an address from this network. This allows communication with hosts that have failed to obtain a DHCP address.

---

# netstat and ss

netstat comes included in all Linux and Windows distributions.

Also se `ss`

netstat is one of the most basic network service debugging tools, telling what ports are open and whether a program is listening on a given port.

The Linux netstat command is being replaced by the ss command, which is capable of displaying more information about network connections.

* `netstat -at` or `ss -at`  
Listing only TCP Transmission Control Protocol port connections
* `netstat -st`  
Showing statistics for TCP
* `netstat -su`  
Showing statistics for UDP
* `netstat -r`  
Display Kernel IP routing table
* `netstat -tp` or `ss -ltp`  
Displaying service name with their PID number



Sources:\
https://www.tecmint.com/20-netstat-commands-for-linux-network-management/

https://www.tecmint.com/ss-command-examples-in-linux/

---

# traceroute

`sudo apt-get install traceroute`

* `traceroute 192.168.2.15`

---

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

# ifconfig

ifconfig comes included in all Linux distribution.

Also see `ip`

ifconfig has been here for a long time and is still used to configure, display and control network interfaces by many. But a new alternative  named `ip` exists on Linux distributions.

* `ifconfig -a`  
  list all interfaces
* `ifconfig eth0 down`  
  Will bring interface eth0 down. Can be usefull after having made configuration changes to an interface.
* `ifconfig eth0 up`  
  Bring interface eth0 up or enable interface eth0.
* `ifconfig eth0 192.168.10.85`  
  Set IP adddress on eth0.  
* `ifconfig eth0 netmask 255.255.255.0`  
  Set subnetmask for eth0. 
* `ifconfig eth0 192.168.2.5 netmask 255.255.255.0 broadcast 192.168.2.255`  
  Set parameters in one command. 
* `ifconfig eth0 promisc`  
  Enable promiscuous mode for eth0. 
* `ifconfig eth0 -promisc`  
  Disable promiscuous mode for eth0. 
* `ifconfig eth0 hw ether 12:34:56:78:90:11`  
  Set temporarily the mac address for eth0. 

Sources:  
* https://www.tecmint.com/ifconfig-vs-ip-command-comparing-network-configuration  
* https://www.computerhope.com/unix/uifconfi.htm  


---

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

# ip

ip comes included in all Linux distribution.

Also see `ifconfig`

* `ip a`  
  List all intefraces.
* `ip route` or `ip r`  
  Display routing table.
* `ip link set eth0 down`  
  Bring interface eth0 down or disable interface eth0.
* `ip link set eth0 up`  
  Bring interface eth0 up or enable interface eth0.
* `ip addr del 192.168.1.15/24 dev eth0`  
  Delete ip address 192.168.1.15/24 from eth0.
* `ip addr flush dev eth0`  
  Delete all IP adresses from eth0.
* `ip route add 10.10.20.0/24 via 192.168.50.100 dev eth0`  
  Add a static route to routing table. 
* `ip route del 122.252.228.38/32`  
  Delete route from routing table.  
* `ip addr flush dev DEVICE`  
  Flush the cache i.e. update route table.
* `ip a add 192.168.80.174/24 dev eth0`  
  Add or set a temporary IP address to interface eth0.

Setting persistent parameters like IP address and route, please see the sources below.

Source: https://www.tecmint.com/ifconfig-vs-ip-command-comparing-network-configuration/

---

# arp  

* `arp`  
  See device arp table with name resolution.
* `arp -n`  
  See device arp table without name resolution.
* `arp -v -n`  
  See what mac addresses are known on the device.
* `arp -a -n`  
See with name resolution what mac addresses are known on the device.
* `arp -a`  
See without name resolution what mac addresses are known on the device.

---

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

# nmcli

`sudo apt-get install network-manager`

nmcli is a command-line tool CLI for Linux NetworkManager. The nmcli can be used to display network device status, create, edit, activate/deactivate, and delete network connections.

* `nmcli`  
  See what network devices are available and list their device names
* `nmcli device`  
  See if interfaces are managed by NetworkManager. Note in the example output below how the Linux device designations eth0 and eth1 here are assosiated with the connection names. There is a distinction between device and connection. The device is the network hardware on the Linux box, e.g. the Ethernet port or the Wi-Fi adapter. A Connection profile is a collection of settings that applies to a device. A device can have multiple Connection profiles. 

  |DEVICE|TYPE|STATE|CONNECTION|
  |------|----|-----|----------|
  |eth0|ethernet|connected|Wired connection 1|
  |eth1|ethernet|connected|Wired connection 2|
  |lo|loopback|unmanaged|--| 
* `nmcli connection edit Wired connection 1`  
  Start network manager connection profile editor

  Her is an example of setting a static IP permanently in the editor:
  * `nmcli> print ipv4`  
  List the IPV4 settings:  
  ['ipv4' setting values]  
ipv4.method:                            auto  
ipv4.dns:                               8.8.8.8  
ipv4.addresses:192.168.11.1/24                 
ipv4.gateway:                           --  
`nmcli> remove ipv4.addresses`  
`nmcli> set ipv4.addresses 192.168.12.25/24`  
Say `yes` to us manual ipv4.method.  
`nmcli> save`  
`nmcli> quit`  
Quit network manager connection profile editor.  
  
Bring down and up the connection, see `ip` or `ifconfig` to do this, and verify that the address is now set.
<br>

* `nmcli connection show Wired connection 1`  
  See what network devices are available with device names
* `nmcli device wifi list`  
Listing available Wi-Fi APs

The Network Manager Graphical Interface Editor can be started with:

* `nm-connection-editor`

Source: `man nmcli-examples`

---

# wireshark

`sudo apt-get install wireshark`

Inwoke by:

`sudo wireshark`

Wireshark is a graphical application which displays captured network packets. Wireshark is able to use ipcap (ip capture) to capture traffic in and/or out of a network interface.
Wireshark can graphically display packets captured by tcpdump.

Also see `tcpdump`

Use filters in Wireshark to capture and/or search for relevant packets.

Filters

* `icmp`  
To see or capture only ICMP traffic i.e. to mainly see ping traffic. 
* `arp or icmp`  
To see or capture only ICMP and ARP traffic.
* `http`  
To see or capture only HTTP traffic.
* `http and tcp.port == 80`  
To see or capture only HTTP traffic for port 80.


---

# nslookup

also see `dig`

* `nslookup dr.dk`  
  Look up the IP address of the domain dr.dk. In this way it can be checked if DNS is set up correctly and working.

---

# dig

also see `nslookup`  

* `dig dr.dk +short`  
  Look up the IP address of the domain dr.dk. In this way it can be checked if DNS is set up correctly and working.

---

# Host misconfiguration or misconnection

Symptoms: Can only ping interfaces on same subnet. Can not ping interfaces on other subnets.

* Possible cause and solution:
    * None or wrongly set default gate way.  
        * Set or correct the default gateway. 

Symptoms: Browser not showing web pages.  

* Possible cause and solution:
    * None or wrong default gate way.  
        * Set or correct the default gateway. 
    * DNS server IP address is not set.  
        * Solution: Set DNS server IP address.

Symptoms: No internet access.  

* Possible cause and solution:
    * No or wrong default gate way.  
        * Set or correct the default gateway. 
    * DNS server IP address is not set.  
        * Solution: Set DNS server IP address.   
    * Wrong IP address.  
        * Set correct IP address.
    * Wrong network mask.
        * Set correct network mask.

Symptoms: Device seems to not reply to incomming ping requests on wired interface

* Possible cause and solution:
    * WIFI is on and replies goes to the WIFI default gateway.
        * On Windows route print will show all known routes. 
        * Use TCP dump or Wireshark to monitor incomming ping requests and replies.
        * Disable WIFI interface.
    * Multiple default gateways in routing table.
        * Linux: The route command will show all default gateways.
        * Solution: Delete the wrong gateways.
        * Solution: Disable interfaces not neede e.g. VMnet interfaces. 


# Router misconfiguration or misconnection

Router interface does nor reply to ping requests.

* Possible cause and solution:
    * Pimg or ICMP not enabled for the interface in security zone.
        * Solution: Enable ping for the interface in security zone.
    * Cable misconnected.
        * Solution: Check cabling against diagram and possibly correct.  

---

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

# Junos

# monitor

also see `arping`

* `monitor traffic detail layer2-headers`  
Due to ICMP packets not being forwarded to the control plane on some Junos devices, ping does not show in the output.
Force arp requests by `arping` to the Junos device. This can be used as a "ping" to Junos devices which do not forward ICMP/ping to the controle plane.  
